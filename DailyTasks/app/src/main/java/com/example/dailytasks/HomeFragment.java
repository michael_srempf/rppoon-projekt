package com.example.dailytasks;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.dailytasks.Database.DatabaseHelper;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class HomeFragment extends Fragment {

    ProgressBar progresFood, progresWater;
    ImageView addWater, addFood, removeWater, removeFood;
    View view;
    TextView tvWaterCurrently, tvFoodCurrently;
    DatabaseHelper databaseHelper;
    RecyclerAdapter recyclerAdapter;
    RecyclerView recyclerView;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, container, false);
        tvWaterCurrently = view.findViewById(R.id.tvWaterCurrently);
        tvFoodCurrently = view.findViewById(R.id.tvFoodCurrently);
        databaseHelper = MainActivity.getDatabase();
        setProgressBar();
        recyclerView = view.findViewById(R.id.RV_tasks);
        recyclerAdapter = RecyclerAdapter.getInstance();
        recyclerView.setAdapter(recyclerAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        recyclerAdapter.refresh();
    }


    private void setProgressBar() {
        progresWater = view.findViewById(R.id.waterProgress);
        progresFood = view.findViewById(R.id.foodProgress);
        progresWater.setMax(30);
        progresFood.setMax(50);
        tvWaterCurrently.setText(databaseHelper.getTodayWater(MainActivity.getDate()));
        tvFoodCurrently.setText(databaseHelper.getTodayFood(MainActivity.getDate()));
        progresWater.incrementProgressBy((int) (Double.parseDouble(databaseHelper.getTodayWater(MainActivity.getDate()))*10));
        progresFood.incrementProgressBy(Integer.valueOf(databaseHelper.getTodayFood(MainActivity.getDate())));
        addWater = view.findViewById(R.id.addWater);
        addFood = view.findViewById(R.id.addFood);
        addWater.setOnClickListener(increaseWater);
        addFood.setOnClickListener(increaseFood);
        removeWater = view.findViewById(R.id.removeWater);
        removeFood = view.findViewById(R.id.removeFood);
        removeWater.setOnClickListener(decreaseWater);
        removeFood.setOnClickListener(decreaseFood);
    }

    private View.OnClickListener decreaseWater = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            progresWater.incrementProgressBy(-1);
            databaseHelper.removeWater(0.1,MainActivity.getDate());
            tvWaterCurrently.setText(databaseHelper.getTodayWater(MainActivity.getDate()));
        }
    };

    private View.OnClickListener decreaseFood = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            progresFood.incrementProgressBy(-10);
            databaseHelper.removeFood(1,MainActivity.getDate());
            tvFoodCurrently.setText(databaseHelper.getTodayFood(MainActivity.getDate()));
        }
    };

    private View.OnClickListener increaseWater = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            progresWater.incrementProgressBy(1);
            databaseHelper.addWater(0.1,MainActivity.getDate());
            tvWaterCurrently.setText(databaseHelper.getTodayWater(MainActivity.getDate()));
        }
    };

    private View.OnClickListener increaseFood = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            progresFood.incrementProgressBy(10);
            databaseHelper.addFood(1,MainActivity.getDate());
            tvFoodCurrently.setText(databaseHelper.getTodayFood(MainActivity.getDate()));
        }
    };

}
