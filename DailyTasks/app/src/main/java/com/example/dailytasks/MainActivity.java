package com.example.dailytasks;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.dailytasks.Database.DatabaseHelper;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity implements TasksDialog.TasksDialogListener {

    private static DatabaseHelper database;
    private static Calendar calendar;
    private static SimpleDateFormat simpleDateFormat;
    private FloatingActionButton floatingActionButton;
    public static DatabaseHelper getDatabase() {
        return database;
    }
    RecyclerAdapter recyclerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        database = new DatabaseHelper(this);
        calendar = Calendar.getInstance();
        simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

        floatingActionButton = findViewById(R.id.addTask);
        recyclerAdapter = RecyclerAdapter.getInstance(this);
        recyclerAdapter.setOnItemClickListener(new RecyclerAdapter.DeleteTaskInterface() {
            @Override
            public void onItemClick(int position) {
                Toast.makeText(MainActivity.this,"Deleted", Toast.LENGTH_LONG).show();
                recyclerAdapter.removeAt(position);
            }
        });
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogTask();
            }
        });
        BottomNavigationView bottomNav = findViewById(R.id.bottom_navigation);
        bottomNav.setOnNavigationItemSelectedListener(navListener);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new HomeFragment()).commit();

    }

    private BottomNavigationView.OnNavigationItemSelectedListener navListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    Fragment selectedFragment = null;

                    switch (item.getItemId()){
                        case R.id.nav_home:
                            selectedFragment = new HomeFragment();
                            break;
                        case R.id.nav_calendar:
                            selectedFragment = new CalendarFragment();
                            break;
                        case R.id.nav_profile:
                            selectedFragment = new ProfileFragment();
                            break;
                    }
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, selectedFragment).addToBackStack(null).commit();
                    return true;
                }
            };
    private void dialogTask(){
        TasksDialog tasksDialog = new TasksDialog();
        tasksDialog.show(getSupportFragmentManager(), "Add task");
    }

    public static String getDate(){
        return simpleDateFormat.format(calendar.getTime());
    }

    @Override
    public void saveTask(String name, String description, String date) {
        database.addTask(name,description,date);
    }
}