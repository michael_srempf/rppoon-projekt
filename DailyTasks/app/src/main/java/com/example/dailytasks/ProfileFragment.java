package com.example.dailytasks;

import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.dailytasks.Database.DatabaseHelper;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class ProfileFragment extends Fragment {

    View view;
    Button waterWeek, waterMonth, foodWeek, foodMont;
    DatabaseHelper database;
    BarChart barChart;
    ArrayList<BarEntry> water;
   SimpleDateFormat simpleDateFormat;
    BarDataSet barDataSet;
    BarData barData;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_profile, container, false);


        waterWeek = view.findViewById(R.id.water7);
        foodWeek = view.findViewById(R.id.meals7);
       waterWeek.setOnClickListener(setWeekWater);
        foodWeek.setOnClickListener(setWeekFood);
        database = MainActivity.getDatabase();
        barChart = view.findViewById(R.id.barChart);
        this.water=new ArrayList<>();


        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_YEAR, -7);
        simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd");
        /*
        Cursor cursor = database.getWaterForDate(simpleDateFormat.format(cal.getTime()),MainActivity.getDate());
        int i=1;
        while (cursor.moveToNext()){
            this.water.add(new BarEntry(i, Float.valueOf(cursor.getString(1))));
            i++;
        }
        barDataSet = new BarDataSet(water,"water last 7 days");
        barDataSet.setColors(ColorTemplate.MATERIAL_COLORS);
        barDataSet.setValueTextColor(Color.BLACK);
        barDataSet.setValueTextSize(16f);
        barData= new BarData(barDataSet);
        barChart.setData(barData);
        */
        return view;
    }

    private View.OnClickListener setWeekWater=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DAY_OF_YEAR, -7);
            Cursor cursor = database.getWaterForDate(simpleDateFormat.format(cal.getTime()),MainActivity.getDate());
            int i=1;
            while (cursor.moveToNext()){
                water.add(new BarEntry(i, Float.valueOf(cursor.getString(1))));
                i++;
            }
            barDataSet = new BarDataSet(water,"Water last 7 days");
            barDataSet.setColors(ColorTemplate.MATERIAL_COLORS);
            barDataSet.setValueTextColor(Color.BLACK);
            barDataSet.setValueTextSize(16f);
            barData= new BarData(barDataSet);
            if(barChart.getData() != null)
                barChart.getData().clearValues();
            barChart.clear();

            barChart.refreshDrawableState();
            barChart.setData(barData);
            barChart.invalidate();
            barChart.notifyDataSetChanged();
        }

    };

    private View.OnClickListener setWeekFood=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DAY_OF_YEAR, -7);
            Cursor cursor = database.getFoodForDate(simpleDateFormat.format(cal.getTime()),MainActivity.getDate());
            int i=1;
            while (cursor.moveToNext()){
                water.add(new BarEntry(i, Float.valueOf(cursor.getString(1))));
                i++;
            }
            barDataSet = new BarDataSet(water,"Food last 7 days");
            barDataSet.setColors(ColorTemplate.MATERIAL_COLORS);
            barDataSet.setValueTextColor(Color.BLACK);
            barDataSet.setValueTextSize(16f);
            barData= new BarData(barDataSet);
            if(barChart.getData() != null)
                barChart.getData().clearValues();
            barChart.clear();

            barChart.refreshDrawableState();
            barChart.setData(barData);
            barChart.invalidate();
            barChart.notifyDataSetChanged();
        }

    };



}
