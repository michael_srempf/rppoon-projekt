package com.example.dailytasks;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.dailytasks.Database.DatabaseHelper;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.RecyclerViewHolder> {


    private Context context;
    DatabaseHelper databaseHelper;
    ArrayList name, description, date;
    private static RecyclerAdapter instance;
    private DeleteTaskInterface listener;
    SimpleDateFormat simpleDateFormat;

    public  interface DeleteTaskInterface{
        void onItemClick(int position);
    }

    public void setOnItemClickListener(DeleteTaskInterface listener){
        this.listener=listener;
    }


    public static RecyclerAdapter getInstance(Context context) {
        if(instance == null){ instance = new RecyclerAdapter(context);}
        return instance;
    }

    public static RecyclerAdapter getInstance() {
        return instance;
    }

    private RecyclerAdapter(Context context) {
        this.context=context;
        this.databaseHelper = MainActivity.getDatabase();
        this.name = new ArrayList<>();
        this.description = new ArrayList<>();
        this.date = new ArrayList<>();
        this.simpleDateFormat=new SimpleDateFormat("d.M.yyyy");
        getData();
    }

    public void setDataForDate(String setdate){
        this.name.clear();
        this.description.clear();
        this.date.clear();
        Cursor cursor = databaseHelper.getTasksForDate(setdate);
        if(cursor.getCount() == 0){
            return;
        }else{
            while (cursor.moveToNext()){
                this.name.add(cursor.getString(1));
                this.description.add(cursor.getString(2));
                this.date.add(cursor.getString(3));
            }
        }
        notifyDataSetChanged();
    }


    void getData(){
        this.name.clear();
        this.description.clear();
        this.date.clear();
        Cursor cursor = databaseHelper.readAllTasks();
        if(cursor.getCount() == 0){
            return;
        }else{
            while (cursor.moveToNext()){
                this.name.add(cursor.getString(1));
                this.description.add(cursor.getString(2));
                this.date.add(cursor.getString(3));
            }
        }
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.task_card, parent, false);
        return new RecyclerViewHolder(view, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolder holder, int position) {
        holder.name_txt.setText(String.valueOf(name.get(position)));
        holder.description_txt.setText(String.valueOf(description.get(position)));


        holder.date_txt.setText(simpleDateFormat.format(Date.valueOf(String.valueOf(date.get(position)))));
    }

    @Override
    public int getItemCount() {
        return name.size();
    }

    public void refresh(){
        getData();
        notifyDataSetChanged();
    }


    public void removeAt(int position){
        databaseHelper.DeleteTaskWhereName(name.get(position).toString());
        getData();
        notifyDataSetChanged();
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        TextView name_txt, description_txt, date_txt;
        ImageView remove;

        public RecyclerViewHolder(@NonNull View itemView, DeleteTaskInterface listener) {
            super(itemView);
            name_txt = itemView.findViewById(R.id.name_of_task);
            description_txt = itemView.findViewById(R.id.description_of_task);
            date_txt = itemView.findViewById(R.id.date_of_task);
            remove = itemView.findViewById(R.id.removeTask);

            remove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener != null){
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION){
                            listener.onItemClick(position);
                        }
                    }
                }
            });
        }

    }
}
