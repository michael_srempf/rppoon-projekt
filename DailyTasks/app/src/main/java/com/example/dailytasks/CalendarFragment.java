package com.example.dailytasks;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.dailytasks.Database.DatabaseHelper;

import java.sql.Date;
import java.text.SimpleDateFormat;

public class CalendarFragment extends Fragment {

    CalendarView calendarView;
    RecyclerView recyclerView;
    DatabaseHelper databaseHelper;
    RecyclerAdapter recyclerAdapter;
    SimpleDateFormat simpleDateFormat;
    TextView selectedDate;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_calendar,container,false);
        calendarView = view.findViewById(R.id.calendar);
        recyclerView = view.findViewById(R.id.RV_tasksOnDate);
        selectedDate = view.findViewById(R.id.selecteddate);
        databaseHelper = MainActivity.getDatabase();
        recyclerAdapter = RecyclerAdapter.getInstance();
        recyclerView.setAdapter(recyclerAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                String date = String.valueOf(year)+"-";
                date+=String.valueOf(month+1);
                date+="-";
                date+=dayOfMonth;
                recyclerAdapter.setDataForDate(date);
            }
        });
        return  view;
    }
}
