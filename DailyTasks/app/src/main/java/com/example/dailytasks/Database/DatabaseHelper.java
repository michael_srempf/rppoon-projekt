package com.example.dailytasks.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.example.dailytasks.ProfileFragment;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME="DailyTasks.db";
    private static final int DATABASE_VERSION=1;
    private static final String TABLE_TASKS = "tasks";
    private static final String TABLE_WATER = "water";
    private static final String TABLE_FOOD = "food";

    private static final String COLUMN_TASKS_ID = "ID";
    private static final String COLUMN_TASKS_NAME = "name";
    private static final String COLUMN_TASKS_DESCRIPTION = "description";
    private static final String COLUMN_TASKS_DATE = "date";

    private static final String COLUMN_WATER_DATE = "date";
    private static final String COLUMN_WATER_AMOUNT = "amount";

    private static final String COLUMN_FOOD_DATE = "date";
    private static final String COLUMN_FOOD_AMOUNT = "amount";


    Context context;
    public DatabaseHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context=context;


    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        String query = "CREATE TABLE "+ TABLE_TASKS +
                " ( " + COLUMN_TASKS_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_TASKS_NAME + " VARCHAR(40)," +
                COLUMN_TASKS_DESCRIPTION + " VARCHAR(80)," +
                COLUMN_TASKS_DATE + " DATE);";
        db.execSQL(query);
        query = "CREATE TABLE "+ TABLE_WATER +
                " ( " + COLUMN_WATER_DATE + " DATE," +
                COLUMN_WATER_AMOUNT + " DECIMAL(3,2)," +
                "CONSTRAINT PK_water PRIMARY KEY (" + COLUMN_WATER_DATE + "));";
        db.execSQL(query);
        query = "CREATE TABLE "+ TABLE_FOOD +
                " ( " + COLUMN_FOOD_DATE + " DATE," +
                COLUMN_FOOD_AMOUNT + " DECIMAL(3,2)," +
                "CONSTRAINT PK_food PRIMARY KEY (" + COLUMN_FOOD_DATE + "));";
        db.execSQL(query);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TASKS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_WATER);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_FOOD);
        onCreate(db);
    }


    public void addTask(String name, String description, String date){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_TASKS_NAME,name);
        cv.put(COLUMN_TASKS_DESCRIPTION,description);
        cv.put(COLUMN_TASKS_DATE,date);
        long result = db.insert(TABLE_TASKS,null,cv);
        if(result == -1){
            Toast.makeText(context, "Failed to insert", Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(context,"Successfully inserted", Toast.LENGTH_LONG).show();
        }
    }

    public boolean doesDateExist(String table_name, String date){
        String query = "SELECT * FROM " + table_name + " WHERE date = '" + date + "';";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if(cursor.moveToFirst()){
            return true;
        }else{
            return false;
        }
    }


    public void addWater(double amount, String date){
        SQLiteDatabase db = this.getWritableDatabase();
        String querry;
        if(doesDateExist(TABLE_WATER,date) == true){
            querry = "UPDATE " + TABLE_WATER + " SET " + COLUMN_WATER_AMOUNT +
                    " = " + COLUMN_WATER_AMOUNT + "+" + amount + " WHERE " +
                    COLUMN_WATER_DATE + " = '" + date + "';";
            db.execSQL(querry);
        }else {
            ContentValues cv = new ContentValues();
            cv.put(COLUMN_WATER_DATE, date);
            cv.put(COLUMN_WATER_AMOUNT, amount);
            db.insert(TABLE_WATER, null, cv);
        }
    }

    public void removeWater(double amount, String date){
        SQLiteDatabase db = this.getWritableDatabase();
        String querry;
        if(doesDateExist(TABLE_WATER,date) == true){
            querry = "UPDATE " + TABLE_WATER + " SET " + COLUMN_WATER_AMOUNT +
                    " = " + COLUMN_WATER_AMOUNT + "-" + amount + " WHERE " +
                    COLUMN_WATER_DATE + " = '" + date + "' AND "+COLUMN_WATER_AMOUNT+">0.1;";
            db.execSQL(querry);
        }
    }

    public void addFood(int amount, String date){
        SQLiteDatabase db = this.getWritableDatabase();
        String querry;
        if(doesDateExist(TABLE_FOOD,date) == true){
            querry = "UPDATE " + TABLE_FOOD + " SET " + COLUMN_FOOD_AMOUNT +
                    " = " + COLUMN_FOOD_AMOUNT + "+" + amount + " WHERE " +
                    COLUMN_FOOD_DATE + " = '" + date + "';";
            db.execSQL(querry);
        }else {
            ContentValues cv = new ContentValues();
            cv.put(COLUMN_FOOD_DATE, date);
            cv.put(COLUMN_FOOD_AMOUNT, amount);
            db.insert(TABLE_FOOD, null, cv);
        }
    }

    public void removeFood(int amount, String date){
        SQLiteDatabase db = this.getWritableDatabase();
        String querry;
        if(doesDateExist(TABLE_FOOD,date) == true){
            querry = "UPDATE " + TABLE_FOOD + " SET " + COLUMN_FOOD_AMOUNT +
                    " = " + COLUMN_FOOD_AMOUNT + "-" + amount + " WHERE " +
                    COLUMN_FOOD_DATE + " = '" + date + "' AND "+COLUMN_FOOD_AMOUNT+">0;";
            db.execSQL(querry);
        }
    }

    public String getTodayWater(String date){
        String querry = "SELECT * FROM "+TABLE_WATER+" WHERE "+COLUMN_WATER_DATE+" = '"+date+"';";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor=null;
        String result = "0";
        if(db!=null){
            cursor = db.rawQuery(querry,null);
        }
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            result = cursor.getString(1);
        }
        return result;
    }

    public String getTodayFood(String date){
        String querry = "SELECT * FROM "+TABLE_FOOD+" WHERE "+COLUMN_FOOD_DATE+" = '"+date+"';";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor=null;
        String result = "0";
        if(db!=null){
            cursor = db.rawQuery(querry,null);
        }
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            result = cursor.getString(1);
        }
        return result;
    }

   public Cursor readAllTasks(){
        String query = "SELECT * FROM "+TABLE_TASKS+" ORDER BY "+COLUMN_TASKS_DATE+" ASC;";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = null;
        if(db != null){
            cursor = db.rawQuery(query,null);
        }
        return cursor;
    }
    public Cursor getTasksForDate(String date){
        String query = "SELECT * FROM "+TABLE_TASKS+" WHERE "+COLUMN_TASKS_DATE+" = '"+date+"';";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = null;
        if(db != null){
            cursor = db.rawQuery(query,null);
        }
        return cursor;
    }

    public Cursor getWaterForDate(String date, String date2){
        String query = "SELECT * FROM "+TABLE_WATER+" WHERE "+COLUMN_WATER_DATE+" BETWEEN '"+date+"' AND '"+date2+"'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = null;
        if(db != null){
            cursor = db.rawQuery(query,null);
        }
        return cursor;
    }

    public Cursor getFoodForDate(String date, String date2){
        String query = "SELECT * FROM "+TABLE_FOOD+" WHERE "+COLUMN_FOOD_DATE+" BETWEEN '"+date+"' AND '"+date2+"'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = null;
        if(db != null){
            cursor = db.rawQuery(query,null);
        }
        return cursor;
    }

    public void insertWater(String date, int value){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("INSERT INTO "+TABLE_WATER+" VALUES ('"+date+"',"+value+");");
    }

    public void insertFood(String date, int value){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("INSERT INTO "+TABLE_FOOD+" VALUES ('"+date+"',"+value+");");
    }

    public void DeleteWaterWhereDate(String date){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM "+TABLE_WATER+" WHERE "+COLUMN_WATER_DATE+" = '"+date+"';");
    }

    public void DeleteFoodWhereDate(String date){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM "+TABLE_FOOD+" WHERE "+COLUMN_FOOD_DATE+" = '"+date+"';");
    }
    public void DeleteAllTasks(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM "+TABLE_TASKS);
    }
    public void DeleteTaskWhereName(String name){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM "+TABLE_TASKS+" WHERE "+COLUMN_TASKS_NAME+" = '"+name+"';");
    }




}
