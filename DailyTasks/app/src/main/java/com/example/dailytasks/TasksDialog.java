package com.example.dailytasks;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.NumberPicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.example.dailytasks.Database.DatabaseHelper;

import java.util.Calendar;

public class TasksDialog extends AppCompatDialogFragment {

    private EditText name, description;
    private NumberPicker day, month, year;
    private TasksDialogListener listener;
    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view =inflater.inflate(R.layout.task_dialog, null);
        builder.setView(view).setTitle("Add task")
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setPositiveButton("Add", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String taskname = name.getText().toString();
                        String desc = description.getText().toString();
                        String date = year.getValue()+"-"+month.getValue()+"-"+day.getValue();
                        listener.saveTask(taskname,desc,date);
                        RecyclerAdapter.getInstance().refresh();
                    }
                });
        Calendar calendar = Calendar.getInstance();
        name = view.findViewById(R.id.edNameOfTask);
        description = view.findViewById(R.id.edDescription);
        day = view.findViewById(R.id.dayPicker);
        day.setMinValue(1);
        day.setMaxValue(calendar.getActualMaximum(calendar.DAY_OF_MONTH));
        day.setValue(calendar.DAY_OF_MONTH);
        month = view.findViewById(R.id.monthPicker);
        month.setMinValue(1);
        month.setMaxValue(12);
        month.setValue(calendar.MONTH);
        year = view.findViewById(R.id.yearPicker);
        year.setMinValue(calendar.get(calendar.YEAR));
        year.setMaxValue(3000);
        return builder.create();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            listener = (TasksDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + "must implement TasksDialogListener");
        }
    }

    public interface TasksDialogListener{
        void saveTask(String name, String description, String date);
    }
}
